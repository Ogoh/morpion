package Morpion;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

public class OrdiTest {

    @Test
    public void gagneAttaqueTest(){
        ArrayList Slist = new ArrayList();
        Slist.add("A");Slist.add("B");
        HashMap hm = new HashMap();
        hm.put('A','X');hm.put('B','X');
        Grille grille = new Grille(hm);
        Joueur j1 = new Joueur();
        Ordi ordi = new Ordi();
        ordi.setCoupsJoues(Slist);
        ordi.tour("attaque", grille, j1);
        //Assert
        Assert.assertTrue(ordi.aGagne());
    }

    @Test
    public void gagneDefenseTest(){
        ArrayList Slist = new ArrayList();
        Slist.add("A");Slist.add("B");
        HashMap hm = new HashMap();
        hm.put('A','O');hm.put('B','O');
        Grille grille = new Grille(hm);
        Joueur j1 = new Joueur();
        Ordi ordi = new Ordi();
        ordi.setCoupsJoues(Slist);
        ordi.tour("defense", grille, j1);
        //Assert
        Assert.assertTrue(ordi.aGagne());
    }

}
