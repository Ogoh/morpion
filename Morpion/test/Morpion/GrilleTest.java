package Morpion;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;


public class GrilleTest {

    @Test
    public void testIsCaseVide() {
        HashMap<Character,Character> grilleCases = new HashMap();
        grilleCases.put('A','X');
        grilleCases.put('C','X');
        Grille grille = new Grille(grilleCases);
        //assert
        Assert.assertTrue(grille.isCaseVide('B'));
        Assert.assertTrue(grille.isCaseVide('D'));
        Assert.assertFalse(grille.isCaseVide('A'));
        Assert.assertFalse(grille.isCaseVide('C'));
    }

    @Test
    public void testAddCoup() {
        Grille grille = new Grille();
        grille.addCoup("AX");
        //assert
        Assert.assertFalse(grille.isCaseVide('A'));
        Assert.assertTrue(grille.isCaseVide('B'));
    }

    @Test
    public void testJoue() {
        Grille grille = new Grille();
        grille.joue('A','X');
        grille.joue('D','X');
        Assert.assertTrue(grille.isCaseVide('B'));
        Assert.assertTrue(grille.isCaseVide('E'));
        Assert.assertFalse(grille.isCaseVide('A'));
        Assert.assertFalse(grille.isCaseVide('D'));
    }

    @Test
    public void testAddSymbol(){
        Grille grille = new Grille();
        Assert.assertTrue(grille.addSymbol('A','X'));
        Assert.assertTrue(grille.addSymbol('C','O'));
        Assert.assertTrue(grille.getSymbol('A')=='X');
        Assert.assertTrue(grille.getSymbol('C')=='O');
        Assert.assertTrue(grille.getSymbol('E')=='&');
    }
}