package Morpion;

import java.util.ArrayList;
import java.util.HashMap;

public class Grille {
    public HashMap<Character,Character> symbolCaseSet;

    public Grille() {
        symbolCaseSet = new HashMap<Character, Character>();
    }

    public Grille(HashMap<Character,Character> symbolCaseSet) {
        this.symbolCaseSet = symbolCaseSet;
    }



    public boolean addCoup(String coup){
        char[] coupChar = coup.toCharArray();
        boolean caseRemplie = joue(coupChar[0],coupChar[1]);
        return caseRemplie;
    }

    public boolean addSymbol(char caseVide,char symbol) {
            symbolCaseSet.put(caseVide,symbol);
            return true;
    }

    public boolean joue(char caseJouee,char symbol){
        switch (caseJouee) {
            case 'A':
                if (symbolCaseSet!=null){
                    for (HashMap.Entry casePrise : symbolCaseSet.entrySet()) {
                        if (casePrise.getKey().equals('A')) {
                            break;
                        }
                    }
                }
                if (symbolCaseSet != null) {
                    symbolCaseSet.put('A',symbol);
                }
                return true;

            case 'B':
                if (symbolCaseSet != null) {
                    for (HashMap.Entry casePrise : symbolCaseSet.entrySet()) {
                        if (casePrise.getKey().equals('B')){
                            break;
                        }
                    }
                }
                if (symbolCaseSet != null) {
                    symbolCaseSet.put('B',symbol);
                    return true;
                }

            case 'C':
                if (symbolCaseSet != null) {
                    for (HashMap.Entry casePrise : symbolCaseSet.entrySet()) {
                        if (casePrise.getKey().equals('C')){
                            break;
                        }
                    }
                }
                if (symbolCaseSet != null) {
                    symbolCaseSet.put('C',symbol);
                    return true;
                }

            case 'D':
                if (symbolCaseSet != null) {
                    for (HashMap.Entry casePrise : symbolCaseSet.entrySet()) {
                        if (casePrise.getKey().equals('D')){
                            break;
                        }
                    }
                }
                if (symbolCaseSet != null) {
                    symbolCaseSet.put('D',symbol);
                    return true;
                }

            case 'E':
                if (symbolCaseSet != null) {
                    for (HashMap.Entry casePrise : symbolCaseSet.entrySet()) {
                        if (casePrise.getKey().equals('E')){
                            break;
                        }
                    }
                }
                if (symbolCaseSet != null) {
                    symbolCaseSet.put('E',symbol);
                    return true;
                }

            case 'F':
                if (symbolCaseSet != null) {
                    for (HashMap.Entry casePrise : symbolCaseSet.entrySet()) {
                        if (casePrise.getKey().equals('F')){
                            break;
                        }
                    }
                }
                if (symbolCaseSet != null) {
                    symbolCaseSet.put('F',symbol);
                    return true;
                }

            case 'G':
                if (symbolCaseSet != null) {
                    for (HashMap.Entry casePrise : symbolCaseSet.entrySet()) {
                        if (casePrise.getKey().equals('G')){
                            break;
                        }
                    }
                }
                if (symbolCaseSet != null) {
                    symbolCaseSet.put('G',symbol);
                    return true;
                }

            case 'H':
                if (symbolCaseSet != null) {
                    for (HashMap.Entry casePrise : symbolCaseSet.entrySet()) {
                        if (casePrise.getKey().equals('H')){
                            break;
                        }
                    }
                }
                if (symbolCaseSet != null) {
                    symbolCaseSet.put('H',symbol);
                }
                return true;

            case 'I':
                if (symbolCaseSet != null) {
                    for (HashMap.Entry casePrise : symbolCaseSet.entrySet()) {
                        if (casePrise.getKey().equals('I')){
                            break;
                        }
                    }
                }
                if (symbolCaseSet != null) {
                    symbolCaseSet.put('I',symbol);
                }
                return true;

            default:
                break;
        }
        return false;
    }

    public boolean isCaseVide(char caseVerif){
        if (symbolCaseSet!=null){
            for (HashMap.Entry casePrise : symbolCaseSet.entrySet()) {
                if (casePrise.getKey().equals(caseVerif)){
                    return false;
                }
            }
        }
        return true;
    }

    public char getSymbol(char caseLettre) {
        for (HashMap.Entry<Character, Character> entry : symbolCaseSet.entrySet()) {
            if (caseLettre == entry.getKey()){
                return entry.getValue();
            }
        }
        return '&';
    }

    public void afficheGrille(){
        ArrayList<Character> listCases = new ArrayList(){{
            add('A');add('B');add('C');add('D');add('E');add('F');add('G');add('H');add('I');
        }};
        for (Character caseGrille : listCases) {
            if (isCaseVide(caseGrille)) System.out.print(caseGrille+"|");
            else{
                System.out.print(getSymbol(caseGrille)+"|");
            }
            if (caseGrille=='C'||caseGrille=='F'||caseGrille=='I'){
                System.out.println();
            }
        }
    }
}
