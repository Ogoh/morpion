package Morpion;

import java.util.ArrayList;

public class Ordi extends Joueur {
    ArrayList<Character> listCases = new ArrayList<Character>(){{
        add('A');add('B');add('C');add('D');add('E');add('F');add('G');add('H');add('I');
    }};

    public Ordi() {}

    public void tour(String strat, Grille grille, Joueur joueur) {
        try {
            Thread.sleep(new Long(2000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (strat.equals("attaque")){
            if (gagneOuEmpeche(grille,joueur)) return;
        }

        if (strat.equals("defense")){

            if (gagneOuEmpeche(grille,joueur)) return;

            if(coupJoueSurCaseVide(grille,'E')) return;

            if (prendreCaseEnCoinSiMilieuPrise(grille)) return;

            if (prendreCaseInterieurSiDeuxCoinsPris(grille,joueur)) return;
        }
    }



    private boolean gagneOuEmpeche(Grille grille, Joueur joueur) {
        String coup;
        coup = aGagneOnTest(grille,this);
        if( coup.equals("true")) {
            this.joue(grille,coup);
            return true;
        }
        coup = aGagneOnTest(grille,joueur);
        if(coup.equals("true")) {
            coup = coup.toCharArray()[0]+super.symbol;
            this.joue(grille,coup);
            return true;
        }
        return false;
    }

    public String aGagneOnTest(Grille grille, Joueur joueur){
        for (char caseGrille: listCases) {
            if (grille.isCaseVide(caseGrille)){
                String coup = caseGrille+joueur.symbol;
                joueur.joue(grille,coup);
                if (joueur.aGagne()){
                    joueur.coupsJoues.remove(Character.toString(caseGrille));
                    grille.symbolCaseSet.remove(caseGrille);
                    return coup;
                }
                else{
                    joueur.coupsJoues.remove(Character.toString(caseGrille));
                    grille.symbolCaseSet.remove(caseGrille);
                }
            }
        }
        return "false";
    }

    public boolean prendreCaseEnCoinSiMilieuPrise(Grille grille){
        if (grille.getSymbol('E') != '&'){
            if(coupJoueSurCaseVide(grille,'A')) return true;
            if(coupJoueSurCaseVide(grille,'C')) return true;
            if(coupJoueSurCaseVide(grille,'G')) return true;
            if(coupJoueSurCaseVide(grille,'I')) return true;
        }
        return false;
    }

    private boolean prendreCaseInterieurSiDeuxCoinsPris(Grille grille, Joueur joueur) {
        int coinsPossedes = 0;
        if(grille.getSymbol('A') == joueur.symbol.toCharArray()[0]) coinsPossedes++;
        if(grille.getSymbol('C') == joueur.symbol.toCharArray()[0]) coinsPossedes++;
        if(grille.getSymbol('G') == joueur.symbol.toCharArray()[0]) coinsPossedes++;
        if(grille.getSymbol('I') == joueur.symbol.toCharArray()[0]) coinsPossedes++;
        if (coinsPossedes>=2){
            if(coupJoueSurCaseVide(grille,'B')) return true;
            if(coupJoueSurCaseVide(grille,'D')) return true;
            if(coupJoueSurCaseVide(grille,'F')) return true;
            if(coupJoueSurCaseVide(grille,'H')) return true;
        }
        return false;
    }

    public boolean coupJoueSurCaseVide(Grille grille,char caseGrille){
        if (grille.getSymbol(caseGrille)=='&'){
            String coup = caseGrille+super.symbol;
            this.joue(grille,coup);
            return true;
        }
        return false;
    }

    public void setCoupsJoues(ArrayList<String> coupsJoues){
        super.coupsJoues = coupsJoues;
    }
}
