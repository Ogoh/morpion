package Morpion;

import java.util.ArrayList;
import java.util.List;

public class Joueur{
    public List<String> coupsJoues;
    public String symbol;

    public Joueur() {
        coupsJoues = new ArrayList<String>();
    }

    public Joueur(ArrayList<String> list) {
        coupsJoues = list;
    }

    public boolean aGagne(){
        for (String coupJoue1: coupsJoues) {
            for (String coupJoue2: coupsJoues) {
                for (String coupJoue3: coupsJoues) {
                    String triplet = coupJoue1+coupJoue2+coupJoue3;
                    if (triplet.equals("ABC") || triplet.equals("DEF") || triplet.equals("GHI") || triplet.equals("ADG") || triplet.equals("BEH") || triplet.equals("CFI") || triplet.equals("AEI") || triplet.equals("CEG" )){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean joue(Grille grille, String coupJoueur) {
        coupsJoues.add(coupJoueur);
        coupJoueur = coupJoueur+symbol;
        return grille.addCoup(coupJoueur);
    }
}

