package Morpion;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);  // Create a Scanner object



        ;
        int nbPartie = 0;
        String rejoue="o";
        while(rejoue.equals("o")){
            Grille grille = new Grille();
            Joueur j1 = new Joueur();
            Ordi ordi = new Ordi();
            if (nbPartie%2==0){
                //Joueur comence
                System.out.println("Vous êtes les X.");
                j1.symbol = "X";
                ordi.symbol = "O";
                while (!j1.aGagne() && !ordi.aGagne()){

                    grille.afficheGrille();

                    //tour Joueur
                    System.out.println("Sur quelle case voulez-vous jouer ?");
                    String coupJoueur = input.next();
                    if(j1.joue(grille, coupJoueur)) {}
                    grille.afficheGrille();

                    //tour Ordi
                    System.out.println("Ordi réfléchit...");
                    ordi.tour("defense", grille, j1);
                }

            }
            else{
                //l'ordi commence
                System.out.println("Vous êtes les O");
                j1.symbol = "O";
                while (!j1.aGagne() && !ordi.aGagne()){

                    //tour Ordi
                    System.out.println("Ordi réfléchit...");
                    ordi.tour("attaque", grille, j1);

                    grille.afficheGrille();

                    //tour Joueur
                    System.out.println("Sur quelle case voulez-vous jouer ?");
                    String coupJoueur = input.next();
                    if(j1.joue(grille, coupJoueur)) {}

                    grille.afficheGrille();


                }

            }
            if (j1.aGagne()){
               System.out.println("Vous avez gagné !");
            }
            else{
                System.out.println("Ordi a gagné ...");
            }
            System.out.println("Voulez-vous rejouer ? (o/n)");
            rejoue = input.next();
            nbPartie++;
        }


    }


}
